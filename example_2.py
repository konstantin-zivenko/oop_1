"""
https://docs.python.org/3/reference/datamodel.html
"""

class Basket:
    def __init__(self, cart, customer):
        self.cart = list(cart)
        self.customer = customer

    def __add__(self, other):
        new_cart = self.cart.copy()
        new_cart.append(other)
        return Basket(new_cart, self.customer)

    def __len__(self):
        return len(self.cart)

    def __str__(self):
        return f"basket of {self.customer} with {self.cart}"

basket1 = Basket(["banana"], "Kostyantin")
print(basket1)
basket1 = basket1 + "apple"

print(basket1)

print(len(basket1))

print(basket1.__len__())